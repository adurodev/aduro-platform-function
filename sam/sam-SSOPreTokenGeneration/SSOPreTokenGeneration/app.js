const https = require('https');

const fetch = async(url) => {
    let dataString = '';
    return await new Promise((resolve, reject) => {
        const req = https.get(url, {}, function(res) {
            res.on('data', chunk => {
                dataString += chunk;
            });
            res.on('end', () => {
                try {
                    resolve(JSON.parse(dataString));
                }
                catch (error) {
                    reject(error);
                }
            });
        });

        req.on('error', (e) => {
            reject(e);
        });
    });
}

const getProfile = async(employeeId, providerName) => {
    const response = await fetch(`${process.env.ES_ENDPOINT}/api/v1/employees/${employeeId}/sso-providers/${providerName}`);
    return response;
}

exports.handler = async(event, context, callback) => {
    console.log(JSON.stringify(event));
    let shouldHandle = false;
    if (!shouldHandle && event.triggerSource === 'TokenGeneration_HostedAuth' && event.request.userAttributes['cognito:user_status'] === 'EXTERNAL_PROVIDER') {
        shouldHandle = true;
    }

    if (!shouldHandle && event.triggerSource === 'TokenGeneration_RefreshTokens' && event.request.userAttributes['cognito:user_status'] === 'EXTERNAL_PROVIDER') {
        shouldHandle = true;
    }

    if (!shouldHandle) {
        callback(null, event);
        return;
    }

    console.log('starting lambda');

    const externalId = event.request.userAttributes['custom:external_id'];

    console.log(externalId);

    let user = null;

    try {
        const [providerName] = event.userName.split('_');
        user = await getProfile(externalId, providerName);
    }
    catch (ex) {
        console.error(ex);
        throw new Error(ex);
    }

    if (!user) {
        throw new Error('We’re having trouble communicating with your company’s SSO provider. Please wait a few moments and try again or contact your IT department for help.');
    }

    const aduroId = user.data ? user.data.id ? user.data.id : '' : '';
    const employeeId = user.data ? user.data.id ? user.data.id : '' : '';
    const employerId =  user.data ? (user.data.employer_id ? user.data.employer_id: '') : '';

    console.log(`aduroId: ${aduroId}`);
    console.log(`employeeId: ${employeeId}`);
    console.log(`externalId: ${externalId}`);

    if (aduroId === '' || employeeId === '' || externalId === '') {
        throw new Error('We’re having trouble communicating with your company’s SSO provider. Please wait a few moments and try again or contact your IT department for help.');
    }
    

    event.response = {
        "claimsOverrideDetails": {
            "claimsToAddOrOverride": {
                "custom:aduro_id": aduroId,
                "custom:employee_id": employeeId,
                "custom:external_id": externalId,
                "custom:employer_id": employerId
            },
            "claimsToSuppress": [""]
        }
    };

    callback(null, event);
};
