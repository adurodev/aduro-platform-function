const https = require('https');

const fetch = async(url) => {
    let dataString = '';
    return await new Promise((resolve, reject) => {
        const req = https.get(url, {}, function(res) {
            res.on('data', chunk => {
                dataString += chunk;
            });
            res.on('end', () => {
                try {
                    resolve(JSON.parse(dataString));
                }
                catch (error) {
                    reject(error);
                }
            });
        });

        req.on('error', (e) => {
            reject(e);
        });
    });
}


const verifyProfileMapping = async(employeeId, providerName) => {
    const response = await fetch(`${process.env.ES_ENDPOINT}/api/v1/employees/${employeeId}/sso-providers/${providerName}/eligible`);
    return response;
}

exports.handler = async(event, context, callback) => {
    console.log(JSON.stringify(event));
    if (event.triggerSource !== 'PostAuthentication_Authentication' || event.request.userAttributes['cognito:user_status'] !== 'EXTERNAL_PROVIDER') {
        callback(null, event);
        return;
    }

    console.log('starting lambda');

    let isSuccess = false;

    const externalId = event.request.userAttributes['custom:external_id'];

    console.log(externalId);

    if (!externalId || !externalId.length) {
        //missing employeId
        callback(new Error("SSO-001: Missing EmployeeId"));
        return;
    }

    const [providerName] = event.userName.split('_');

    try {
        const result = await verifyProfileMapping(externalId, providerName);
        console.log(JSON.stringify(result));

        isSuccess = result.data.is_eligible;

        if (!isSuccess) {
            //combination issue
            callback(new Error("SSO-002: There is no combination for Employee & Provider Name"));
            return;
        }
        else {
            callback(null, event);
        }
    }
    catch (e) {
        console.error(e);
        throw new Error(e);
    }
};
