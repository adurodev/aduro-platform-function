exports.handler = async(event, context, callback) => {
    console.log(JSON.stringify(event));
    if (event.triggerSource !== 'PreSignUp_ExternalProvider') {
        callback(null, event);
        return;
    }

    console.log('starting lambda');

    const externalId = event.request.userAttributes['custom:external_id'];

    console.log(externalId);

    if (!externalId || !externalId.length) {
        //missing employeId
        callback(new Error("SSO-001: Missing EmployeeId"));
        return;
    }

    return callback(null, event);

};
