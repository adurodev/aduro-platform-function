exports.handler = async (event, context, callback) => {
    return {
        "applinks": {
            "apps": [],
            "details": [
                {
                    "appID": "DMHF3K9T23.www.aduro.amp",
                    "paths": [
                        "/referral/castlight/*",
                        "/v1/redirect/clients/*/activities/*/teams/*/invitations/*"
                    ]
                }
            ]
        }
    };
};