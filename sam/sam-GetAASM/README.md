# sam-GetAASM

## Infrastructure requirements:
- VPC
- Subnets
- Security Groups

## Roles:
- Cognito
- Cloud Watch

## Evironment Variables:
- ES_ENDPOINT: apigateway point to ES Service, for example: https://es-api-gateway-endpoint
